package transactions.message_handler;

import java.util.Objects;

public class ProductType implements IProductType {

    private String name;

    public ProductType(String name) {
        if (name == null || "".equals(name)) {
            throw new IllegalArgumentException("Name of product must not be null or an empty string");
        } else {
            this.name = name.trim().toUpperCase();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductType that = (ProductType) o;
        return name.equalsIgnoreCase(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "ProductType{" +
                "name='" + name + '\'' +
                '}';
    }
}
