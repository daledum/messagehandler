package transactions.message_handler;

public class Adjustment implements IAdjustment {

    private Operation operation;
    private ISale sale;

    protected Adjustment(Operation operation, ISale sale) {
        this.operation = operation;
        this.sale = sale;
    }

    @Override
    public Operation getOperation() {
        return operation;
    }

    @Override
    public ISale getSale() {
        return sale;
    }

    @Override
    public String toString() {
        return "Adjustment{" +
                "operation=" + operation +
                ", sale=" + sale +
                '}';
    }
}
