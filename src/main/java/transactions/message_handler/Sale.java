package transactions.message_handler;

public class Sale implements ISale {

    private int occurrences;
    private IProductType productType;
    private long value;
    private static final int SINGLE = 1;

    protected Sale(IProductType productType, long value) {
        this.occurrences = SINGLE;
        this.productType = productType;
        this.value = value;
    }

    protected Sale(int occurrences, IProductType productType, long value) {
        this.occurrences = occurrences;
        this.productType = productType;
        this.value = value;
    }

    @Override
    public int getOccurrences() {
        return occurrences;
    }

    @Override
    public long getValue() {
        return value;
    }

    @Override
    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public IProductType getProductType() {
        return productType;
    }

    @Override
    public String toString() {

        if (SINGLE == occurrences) {
            return "Sale{" +
                    "productType=" + productType +
                    ", value=" + value +
                    '}';
        }

        return "Sale{" +
                "occurrences=" + getOccurrences() +
                ", productType=" + productType +
                ", value=" + value +
                '}';
    }
}
