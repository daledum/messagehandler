package transactions.message_handler;

public interface IAdjustment {
    Operation getOperation();
    ISale getSale();
}
