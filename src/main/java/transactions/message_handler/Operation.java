package transactions.message_handler;

public enum Operation {
    ADD,
    SUBTRACT,
    MULTIPLY
}
