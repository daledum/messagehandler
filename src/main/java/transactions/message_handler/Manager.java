package transactions.message_handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Manager {

    private class Pair<X, Y> {
        private X count;
        private Y total;
        private Pair(X count, Y total) {
            this.count = count;
            this.total = total;
        }
    }

    private final List<ISale> sales;
    private final List<IAdjustment> adjustments;
    private int messageCount;
    private static final int MAX = 50;
    // Map with each product type received as key, how many times it was received and its total sale value
    private final HashMap<IProductType, Pair<Integer, Long>> repository;

    public Manager() {
        this.sales = new ArrayList<>();
        this.adjustments = new ArrayList<>();
        messageCount = 0;
        repository = new HashMap<>();
    }

    protected void handle(IMessage message) {
        messageCount++;

        if (messageCount <= MAX) {

            IAdjustment adjustment = message.getAdjustment();
            ISale sale = message.getSale();

            if (adjustment != null) {
                // Record the adjustment (which is not a sale in the proper meaning of the word)
                adjustments.add(adjustment);
                // Process what was received
                process(adjustment);
            }

            if (sale != null) {
                // Record the sale
                sales.add(sale);
                // Process the sale
                process(sale);
            }

            if (messageCount % 10 == 0) {

                if (repository.isEmpty()) {
                    System.out.println("No single or multiple sales done");
                } else {
                    // ProductType's constructor does not accept null, so should be fine to print like this
                    System.out.println("The number of sales of each product and their total value: ");
                    for (Map.Entry<IProductType, Pair<Integer, Long>> entry : repository.entrySet()) {
                        System.out.println(entry.getKey());
                        Pair<Integer, Long> valuePair = entry.getValue();
                        System.out.println("NumberOfSales={" + valuePair.count +
                                "}, TotalValue={" + valuePair.total +
                                "} ");
                    }
                }

            }

            if (messageCount == MAX) {
                System.out.println("App is pausing, no new messages accepted");
                System.out.println("The adjustments that have been made:");
                if (adjustments.isEmpty()) {
                    System.out.println("No adjustments done");
                } else {
                    for (IAdjustment adj: adjustments) {
                        System.out.println(adj);
                    }
                }
            }
        }
    }

    private void process(ISale sale) {

        final int occurrences = sale.getOccurrences();
        final IProductType productType = sale.getProductType();

        // Update the repository containing the product type and the sale count and total value
        // Not using containsKey (keys are a set), due to the fact that it uses .hashCode before .equals for equality
        Pair<Integer, Long> valuePair = null;
        for (Map.Entry<IProductType, Pair<Integer, Long>> entry : repository.entrySet()) {
            if (entry.getKey().equals(productType)) {
                valuePair = entry.getValue();
                break;
            }
        }

        if (valuePair == null) {
            // This means that the repo doesn't have this product type, yet
            repository.put(productType, new Pair<>(occurrences, occurrences * sale.getValue()));
        } else {
            valuePair.count += occurrences;
            valuePair.total += occurrences * sale.getValue();
        }
    }

    private void process(IAdjustment adjustment) {

        ISale adjustmentSale = adjustment.getSale();
        boolean prodTypeIsInSales = false;

        for (ISale sale: sales) {
            if (sale.getProductType().equals(adjustmentSale.getProductType())) {
                prodTypeIsInSales = true;

                switch (adjustment.getOperation()) {
                    case ADD: {
                        sale.setValue(sale.getValue() + adjustmentSale.getValue());
                        break;
                    }

                    case SUBTRACT: {
                        sale.setValue(sale.getValue() - adjustmentSale.getValue());
                        break;
                    }

                    case MULTIPLY: {
                        sale.setValue(sale.getValue() * adjustmentSale.getValue());
                        break;
                    }
                    default: break;
                }
            }
        }

        // Method processing normal (non adjustment) sales makes sure that if the product type is in the sale,
        // it gets added to the repo
        if (prodTypeIsInSales) {
            for (Map.Entry<IProductType, Pair<Integer, Long>> entry : repository.entrySet()) {
                long sum = 0L;

                for (ISale sale: sales) {
                    if (sale.getProductType().equals(entry.getKey())) {
                        sum += sale.getValue();
                    }
                }
                entry.getValue().total = sum;
            }
        }
    }
}
