package transactions.message_handler;

public class Message implements IMessage {

    private ISale sale;
    private IAdjustment adjustment;

    protected Message(ISale sale) {
        this.sale = sale;
        adjustment = null;
    }

    protected Message(IAdjustment adjustment) {
        this.sale = null;
        this.adjustment = adjustment;
    }

    // We could also have a message containing a sale and an adjustment-sale
//    protected SaleMessage(ISale sale, IAdjustment adjustment) {
//        this.sale = sale;
//        this.adjustment = adjustment;
//    }

    @Override
    public ISale getSale() {
        return sale;
    }

    @Override
    public IAdjustment getAdjustment() {
        return adjustment;
    }

    @Override
    public String toString() {

        if (sale == null && adjustment != null) {
            return "SaleMessage{" +
                    "adjustment= " + adjustment +
                    "} ";
        }

        if (sale != null && adjustment == null) {
            return "SaleMessage{" +
                    "sale=" + sale +
                    "} ";
        }

        return "SaleMessage{" +
                "sale=" + sale +
                "adjustment= " + adjustment +
                "} ";
    }
}
