package transactions.message_handler;

/**
 * Main class of the app that needs to be run
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // TODO: Use protected instead of public + write unit tests
        System.out.println( "Messages are being created and processed: ");
        Manager manager = new Manager();

        // A method is not used here for complete control on what messages are being created
        // 10 Blocks of 5 messages
        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        manager.handle(new Message(new Sale(new ProductType("apples"), 20)));
        manager.handle(new Message(new Sale(new ProductType("oranges"), 10)));
        manager.handle(new Message(new Sale(4, new ProductType("kiwis"), 30)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
        manager.handle(new Message(
                new Adjustment(Operation.MULTIPLY, new Sale(new ProductType("apples"), 2))));

        // Plus 2 Extra
        manager.handle(new Message(new Sale(new ProductType("oranges"), 5)));
        manager.handle(new Message(
                new Adjustment(Operation.ADD, new Sale(new ProductType("oranges"), 44))));
    }
}
