package transactions.message_handler;

public interface ISale {
    int getOccurrences();
    long getValue();
    void setValue(long value);
    IProductType getProductType();
}
