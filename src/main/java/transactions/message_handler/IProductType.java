package transactions.message_handler;

public interface IProductType {
    String getName();
}
