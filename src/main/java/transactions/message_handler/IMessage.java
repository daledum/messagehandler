package transactions.message_handler;

public interface IMessage {
    ISale getSale();
    IAdjustment getAdjustment();
}
